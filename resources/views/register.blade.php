@extends('adminlte.master')

@section('title', 'Register')

@section('content')

<div class="container">
    <form method="POST" action='/welcome'>
        @csrf
      <label>First name: <br><br>
        <input type="text" name="firstname">
      </label>
      <br><br>

      <label>Last name: <br><br>
        <input type="text" name="lastname">
      </label><br><br>

      <label>Gender: <br><br>
        <input type="radio" name="gender" value="0"> Male <br>
        <input type="radio" name="gender" value="1"> Female <br>
        <input type="radio" name="gender" value="2"> Other <br>
      </label><br><br>

      <label>Nationality: <br><br>
        <select>
          <option value="0">Indonesian</option>
          <option value="1">American</option>
          <option value="2">England</option>
        </select>
      </label> <br><br>

      <label>Language Spoken: <br><br>
        <input type="checkbox" name="bahasa" value="0">Bahasa indonesia <br>
        <input type="checkbox" name="english" value="1">English <br>
        <input type="checkbox" name="other" value="2">Other <br>
      </label><br>

      <label>Bio: <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
      </label><br>

      <input type="submit" value="kirim">
    </form>
</div>
    @endsection
