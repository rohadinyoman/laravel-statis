<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('register');
    }
    public function kirim(Request $request)
    {
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        return view('welcome', compact('firstname', 'lastname'));
    }
    public function welcome()
    {
        return view('welcome');
    }
}
