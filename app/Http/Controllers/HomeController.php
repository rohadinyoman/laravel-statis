<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('/home');
    }
    public function master()
    {
        return view('/adminlte/master');
    }
    public function index()
    {
        return view('/items/index');
    }
    public function table()
    {
        return view('/items/table');
    }
    public function dataTable()
    {
        return view('/items/data-tables');
    }
}
