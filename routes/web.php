<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@kirim');
Route::get('/welcome', 'AuthController@welcome');

Route::get('/adminlte/master', 'HomeController@master');
Route::get('/items/index', 'HomeController@index');
Route::get('/', 'HomeController@table');
Route::get('/data-tables', 'HomeController@dataTable');
